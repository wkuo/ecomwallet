DROP TABLE IF EXISTS wallet ;
CREATE TABLE wallet (
    id bigint PRIMARY KEY,
    balance double,
    ct_dt timestamp,
    lm_dt timestamp
) ;