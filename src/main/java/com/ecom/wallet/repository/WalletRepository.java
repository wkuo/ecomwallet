package com.ecom.wallet.repository;
import org.springframework.data.repository.CrudRepository;

import com.ecom.wallet.model.Wallet;

public interface WalletRepository extends CrudRepository<Wallet, Long>{


}
