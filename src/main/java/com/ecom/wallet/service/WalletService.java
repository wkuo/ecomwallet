package com.ecom.wallet.service;

import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ecom.wallet.model.Wallet;
import com.ecom.wallet.model.WalletWS;
import com.ecom.wallet.repository.WalletRepository;
import com.ecom.wallet.util.MoneyUtil;



@Service
public class WalletService implements IWalletService {
	
	@Autowired
	WalletRepository repo;
	
	private static final Logger log = LoggerFactory.getLogger(WalletService.class);

	
	public List<?> findAll() {
		return (List<?>) repo.findAll();
	}

	@Override
	public Wallet find(Long id) {
		return repo.findById(id).orElse(null);
	}
	
	@Override
	public void deductBalance(WalletWS wws) {
		Long id = Long.valueOf(wws.getId());
		log.info("Service : Wallet " + id + " deducting " + wws.getAmount());
		Wallet w = repo.findById(id).orElse(null);
		if (null != w) {
			Double temp = w.getBalance();
			BigDecimal current = MoneyUtil.doubleToMoney(temp) ;
			BigDecimal newBal = current.subtract(MoneyUtil.doubleToMoney(wws.getAmount()));
			if ( 0 < BigDecimal.ZERO.compareTo(newBal)) {
				// Insufficient fund, stop transaction and abort
				throw new IllegalStateException("Insufficient Fund");
			} else {
				w.setBalance(newBal.doubleValue());
				repo.save(w);
			}
		} else {
			throw new IllegalStateException("Wallet not found");
		}
	}
	
	@Override
	public void addBalance(WalletWS wws) {
		Long id = Long.valueOf(wws.getId());
		log.info("Service : Wallet " + id + " depositing " + wws.getAmount());
		Wallet w = repo.findById(id).orElse(null);
		if (null != w) {
			BigDecimal newBal = MoneyUtil.doubleToMoney(w.getBalance()).add(MoneyUtil.doubleToMoney(wws.getAmount())) ;
			w.setBalance(newBal.doubleValue());
			repo.save(w);
		} else {
			throw new IllegalStateException("Wallet not found");
		}
	}

}
