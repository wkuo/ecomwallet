package com.ecom.wallet.service;

import java.util.List;

import com.ecom.wallet.model.Wallet;
import com.ecom.wallet.model.WalletWS;

public interface IWalletService {

	void deductBalance(WalletWS w);
	
	void addBalance(WalletWS w);

	Wallet find(Long id);
	
	List<?> findAll();

}
