package com.ecom.wallet.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;


@Entity
@Table(name = "wallet")
public class Wallet {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id; // User Id
	
	private Double balance;
	
	@CreatedDate
	@Column(name = "ct_dt")
	private Date ctDt;
	
	@LastModifiedDate
	@Column(name = "lm_dt")
	private Date lmDt;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getBalance() {
		return balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	public Date getCtDt() {
		return ctDt;
	}

	public void setCtDt(Date ctDt) {
		this.ctDt = ctDt;
	}

	public Date getLmDt() {
		return lmDt;
	}

	public void setLmDt(Date lmDt) {
		this.lmDt = lmDt;
	}

}
