package com.ecom.wallet.util;

import java.math.BigDecimal;

public class MoneyUtil {
	
	public static BigDecimal doubleToMoney(Double amt) {
		return new BigDecimal(amt.toString());
	}
	
	
}
