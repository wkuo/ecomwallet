package com.ecom.wallet.controller;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.stereotype.Component;

import com.ecom.wallet.model.WalletWS;

@Component
public class WalletRoute extends RouteBuilder {

	@Override
	public void configure() throws Exception {

		rest().get("/wallet")
			.route()
			.bean("walletService", "findAll")
			.marshal().json(JsonLibrary.Jackson);


		rest().put("/wallet/deduct/{id}")
			.route()
			.unmarshal().json(JsonLibrary.Jackson, WalletWS.class)
			//.to("bean-validator:validatePayment")
			.bean("walletService", "deductBalance")
				.marshal().json(JsonLibrary.Jackson);
		
		rest().put("/wallet/add/{id}")
			.route()
			.unmarshal().json(JsonLibrary.Jackson, WalletWS.class)
			//.to("bean-validator:validatePayment")
			.bean("walletService", "addBalance")
			.marshal().json(JsonLibrary.Jackson);
		
		}

}
